//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by StopwatchCEppc.rc
//
#define IDS_APP_TITLE                   1
#define IDC_STOPWATCHCE                 2
#define IDI_STOPWATCHCE                 101
#define IDR_MENU                        102
#define IDS_OK                          103
#define IDS_HELP                        104
#define IDS_OPTIONS                     104
#define IDD_ABOUTBOX                    105
#define IDD_ABOUTBOX_WIDE               106
#define IDS_STARTSTOP                   129
#define IDC_STATIC_1                    201
#define IDC_STATIC_2                    202
#define IDC_STATIC_3                    203
#define IDC_STATIC_4                    204
#define ID_HELP_CLOCKMODE               32771
#define ID_CLOCKMODE                    32772
#define IDM_CLOCKMODE                   32773
#define ID_START                        32774
#define ID_OPTIONS_RESET                32775
#define IDM_RESET                       32776
#define ID_OPTIONS_START                32777
#define ID_OPTIONS_SPLIT                32778
#define ID_OPTIONS_STOP                 32779
#define IDM_START                       32780
#define IDM_SPLIT                       32781
#define IDM_STOP                        32782
#define ID_OPTIONS_SPLITTIMING          32783
#define IDM_SPLIT_TIMING                32784
#define IDM_ABOUT                       32785
#define IDM_STARTSPLITSTOP              32786
#define IDM_OK                          40000
#define IDM_HELP                        40001
#define IDM_HELP_ABOUT                  40002
#define IDM_STARTSTOP                   40003
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32787
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
