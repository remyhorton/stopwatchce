/*
  StopwatchCE - Digit display routines
  SPDX-License-Identifier: GPL-3.0-only
  Copyright(c) Remy Horton, 2018
*/
#include "stdafx.h"
#include "StopwatchCE.h"

void setSegH(POINT *pts, int x, int y, int width, int length, int gap, int flip)
{
#pragma region
pts[0].x = gap+x;
pts[0].y = y;
pts[1].x =-gap+x+length;
pts[1].y = y;
pts[2].x =-gap+x+length-width;
pts[2].y = y+width;
pts[3].x = gap+x       +width;
pts[3].y = y+width;
if( flip )
	pts[2].y = pts[3].y = y-width;
#pragma endregion
}

void setSegV(POINT *pts, int x, int y, int width, int length, int gap, int flip)
{
#pragma region
pts[0].x = x;
if( flip )
	{
	pts[1].x = x-width;
	pts[2].x = x-width;
	}
else
	{
	pts[1].x = x+width;
	pts[2].x = x+width;
	}
pts[3].x = x;

pts[0].y = y+gap;
pts[1].y = y+gap+width;
pts[2].y = y-gap-width+length;
pts[3].y = y-gap      +length;
#pragma endregion
}

void setSegs(SegSet_t *segSet, int xPos, int yPos, int sLen, int sGap, int sWid)
{
#pragma region
setSegH(&segSet->segments[0][0], xPos,     yPos,       sWid,sLen,sGap, 0);
setSegV(&segSet->segments[1][0], xPos,     yPos,       sWid,sLen,sGap, 0);
setSegV(&segSet->segments[2][0], xPos+sLen,yPos,       sWid,sLen,sGap, 1);

setSegH(&segSet->segments[3][0], xPos,     yPos+sLen-(sGap)     ,sWid,sLen,sGap, 0);
setSegV(&segSet->segments[4][0], xPos,     yPos+sLen-(sGap)     ,  sWid,sLen,sGap, 0);
setSegV(&segSet->segments[5][0], xPos+sLen,yPos+sLen-(sGap)     ,  sWid,sLen,sGap, 1);

setSegH(&segSet->segments[6][0], xPos,     yPos+sLen*2-(sGap)   ,sWid,sLen,sGap, 1);
#pragma endregion
}

void drawSegs(HDC hdc, SegSet_t *segSet, const unsigned char setSegments)
{
#pragma region
if( setSegments & 1 )
	Polygon(hdc, &segSet->segments[0][0], 4);
if( setSegments & 2 )
	Polygon(hdc, &segSet->segments[1][0], 4);
if( setSegments & 4 )
	Polygon(hdc, &segSet->segments[2][0], 4);
if( setSegments & 8 )
	Polygon(hdc, &segSet->segments[3][0], 4);
if( setSegments & 16 )
	Polygon(hdc, &segSet->segments[4][0], 4);
if( setSegments & 32 )
	Polygon(hdc, &segSet->segments[5][0], 4);
if( setSegments & 64 )
	Polygon(hdc, &segSet->segments[6][0], 4);
#pragma endregion
}

int calcDigitCoords(const int windowWidth)
{
#pragma region
int baseSize = windowWidth / 60 ;
int x = 0;
int y = 4;
int baseThick = windowWidth / 40;
//FIXME: Non-fixed gaps and segment widths..
//IDEA: Lookup hand-tuned indexed on width?
x = baseSize * 5;
setSegs(&Global.digitCoords[0], x, y, baseSize*7,2,baseThick);
x += baseSize * 9;
setSegs(&Global.digitCoords[1], x, y, baseSize*7,2,baseThick);
x += baseSize * 12;
setSegs(&Global.digitCoords[2], x, y, baseSize*7,2,baseThick);
x += baseSize * 9;
setSegs(&Global.digitCoords[3], x, y, baseSize*7,2,baseThick);
x += baseSize * 10;
baseThick /= 2;
setSegs(&Global.digitCoords[4], x, y, baseSize*3,1,baseThick);
x += baseSize * 5;
setSegs(&Global.digitCoords[5], x, y, baseSize*3,1,baseThick);

y += baseSize*14 + 4;
return y;
#pragma endregion
}

void setDigitSegments(int digitIdx, int value)
{
#pragma region
unsigned char bits;
switch( value )
	{
	case 1:	
		bits = 4 | 32;
		break;
	case 2:
		bits = 1 | 4 | 8 | 16 | 64;
		break;
	case 3:
		bits = 1 | 4 | 8 | 32 | 64;
		break;
	case 4:
		bits = 2 | 4 | 8 | 32;
		break;
	case 5:
		bits = 1 | 2 | 8 | 43 | 64;
		break;
	case 6:
		bits = 1 | 2 | 8 | 16 | 32 | 64;
		break;
	case 7:
		bits = 1 | 4 | 32 ;
		break;
	case 8:
		bits = 1 | 2 | 4 | 8 | 16 | 32 | 64;
		break;
	case 9:
		bits = 1 | 2 | 4 | 8 | 32 | 64;
		break;
	case 0:
		bits = 1 | 2 | 6 | 16 | 32 | 64;
		break;
	case 10:
		bits = 0; // Unlit 
		break;
	default: 
		bits = 1 | 8 | 64 | (16|2); // E
	}
Global.digitBits[digitIdx] = bits;
#pragma endregion
}

LRESULT CALLBACK PaintProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#pragma region
HDC hdc;
PAINTSTRUCT ps;

if( Global.memDC == NULL )
	{
	hdc = GetDC(hwnd);
	Global.memDC = CreateCompatibleDC(hdc);
	int iWidth = Global.digitBox.right-Global.digitBox.left;
	int iHeight= Global.digitBox.bottom;
	Global.memDCbitmap = CreateCompatibleBitmap(hdc, iWidth, iHeight);
	ReleaseDC(hwnd, hdc);
	}

SelectObject(Global.memDC,Global.memDCbitmap);

FillRect(Global.memDC, &Global.digitBox, Global.brush[1]);
SelectObject(Global.memDC, Global.pen );
SelectObject(Global.memDC, Global.brush[0] );
drawSegs(Global.memDC, &Global.digitCoords[0], Global.digitBits[0]);
drawSegs(Global.memDC, &Global.digitCoords[1], Global.digitBits[1]);
drawSegs(Global.memDC, &Global.digitCoords[2], Global.digitBits[2]);
drawSegs(Global.memDC, &Global.digitCoords[3], Global.digitBits[3]);
drawSegs(Global.memDC, &Global.digitCoords[4], Global.digitBits[4]);
drawSegs(Global.memDC, &Global.digitCoords[5], Global.digitBits[5]);

hdc = BeginPaint(hwnd,&ps);
BitBlt(hdc, 0,0, 
	   Global.digitBox.right-Global.digitBox.left, 
	   Global.digitBox.bottom, Global.memDC, 0,0, SRCCOPY);
EndPaint(hwnd,&ps);

//ReleaseDC(hwnd, hDC);
return 0;
#pragma endregion
}


LRESULT CALLBACK TimerProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
//	TimerProc - Handle timer callbacks
//	wParam = 
//		1: Clock update
//		2: Stopwatch update
//		3: Not a real timer. Force update when stopwatch stopped
{
#pragma region
SYSTEMTIME sysTime;
if( wParam == 1 && Global.time.clockMode )
	GetLocalTime(&sysTime);
else if( wParam == 2 && !Global.time.clockMode )
	{
	#pragma region Timer active
	DWORD timeElapsed = Global.time.totalElapsed + 
		getSystemMillis() - Global.time.epoch;
	sysTime.wHour     =(WORD)(  (timeElapsed) / (1000 * 60) );
	timeElapsed -= sysTime.wHour * 1000 * 60;
	sysTime.wMinute   =(WORD)(  timeElapsed / 1000 );
	timeElapsed -= sysTime.wMinute * 1000;
	sysTime.wSecond   =(WORD)(  timeElapsed / 10 );
	#pragma endregion Comment
	}
else if( wParam == 3 )
	{		
	#pragma region Forceupdate
	DWORD timeElapsed = Global.time.totalElapsed;
	sysTime.wHour     = (WORD)( (timeElapsed) / (1000 * 60) );
	timeElapsed -= sysTime.wHour * 1000 * 60;
	sysTime.wMinute   = (WORD)( timeElapsed / 1000 );
	timeElapsed -= sysTime.wMinute * 1000;
	sysTime.wSecond   = (WORD)( timeElapsed / 10 );
	#pragma endregion Force stopwatch display update
	}
else
	return 0;

if( sysTime.wHour > 99 ) 
	{
	sysTime.wSecond = sysTime.wMinute;
	sysTime.wMinute = sysTime.wHour % 60;
	sysTime.wHour   = sysTime.wHour / 60;
	}

setDigitSegments(0, sysTime.wHour / 10);
setDigitSegments(1, sysTime.wHour % 10);
setDigitSegments(2, sysTime.wMinute / 10);
setDigitSegments(3, sysTime.wMinute % 10);
setDigitSegments(4, sysTime.wSecond / 10);
//setDigitSegments(5, 10);
setDigitSegments(5, sysTime.wSecond % 10);
InvalidateRect(hwnd, &Global.digitBox, false);				
return 0;
#pragma endregion
}

