/*
  StopwatchCE - Entrypoint for application
  SPDX-License-Identifier: GPL-3.0-only
  Copyright(c) Remy Horton, 2018
*/
#include "stdafx.h"
#include "StopwatchCE.h"

#define MAX_LOADSTRING 128

Global_t Global;


DWORD getSystemMillis()
{
return GetTickCount();
}


BOOL isSplitTiming()
{
HMENU hMenu = (HMENU)SendMessage(Global.menuBar, SHCMBM_GETSUBMENU,
	(WPARAM)0, (LPARAM)IDM_HELP);
MENUITEMINFO menuInfo;
memset(&menuInfo, 0, sizeof(MENUITEMINFO));
menuInfo.cbSize = sizeof(MENUITEMINFO);
menuInfo.fMask = MIIM_STATE;
GetMenuItemInfo(hMenu, IDM_SPLIT_TIMING, false, &menuInfo);
if( menuInfo.fState & MFS_CHECKED )
	return TRUE;
else
	return FALSE;
}

LRESULT CALLBACK CommandProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#pragma region
HMENU hMenu;
MENUITEMINFO menuInfo;
TBBUTTONINFO btnInfo;
int iIndex;

switch( msg )
	{
    case WM_COMMAND:
		switch( LOWORD(wParam) )
			{
			case IDM_ABOUT:
				DialogBox(Global.mainWnd.hInst, (LPCTSTR)IDD_ABOUTBOX, hwnd, About);
                break;

			case IDM_CLOCKMODE:
				#pragma region Clockmode toggle
				hMenu = (HMENU)SendMessage(Global.menuBar, SHCMBM_GETSUBMENU,
					(WPARAM)0, (LPARAM)IDM_HELP);
				btnInfo.cbSize = sizeof(btnInfo);
				btnInfo.dwMask = TBIF_TEXT;
				if( Global.time.clockMode )
					{
					Global.time.clockMode = false;
					CheckMenuItem(hMenu, IDM_CLOCKMODE, MF_BYCOMMAND | MF_UNCHECKED);
					KillTimer(hwnd, 1);
					SendMessage(hwnd, WM_TIMER, (WPARAM)3, 0);
					if( isSplitTiming() )
						btnInfo.pszText = TEXT("Start/Split");
					else
						btnInfo.pszText = TEXT("Start/Stop");
					}
				else
					{
					SendMessage(hwnd, WM_COMMAND, (WPARAM)IDM_STOP, 0);
					Global.time.clockMode = true;
					CheckMenuItem(hMenu, IDM_CLOCKMODE, MF_BYCOMMAND | MF_CHECKED);
					SetTimer(hwnd, 1, 250, NULL);
					SendMessage(hwnd, WM_TIMER, (WPARAM)1, 0);
					btnInfo.pszText = TEXT("Save Time");
					}			
				SendMessage(Global.menuBar, TB_SETBUTTONINFO, 
					(LPARAM)IDM_STARTSPLITSTOP, (LPARAM)&btnInfo);
				#pragma endregion
				break;

			case IDM_SPLIT_TIMING:
				#pragma region Split timing menuitem toggle
				btnInfo.cbSize = sizeof(btnInfo);
				btnInfo.dwMask = TBIF_TEXT;
				hMenu = (HMENU)SendMessage(Global.menuBar, SHCMBM_GETSUBMENU,
					(WPARAM)0, (LPARAM)IDM_HELP);					
				menuInfo.cbSize = sizeof(MENUITEMINFO);
				menuInfo.fMask = MIIM_STATE;
				GetMenuItemInfo(hMenu, IDM_SPLIT_TIMING, false, &menuInfo);
				if( menuInfo.fState & MFS_CHECKED )
					{
					CheckMenuItem(hMenu, IDM_SPLIT_TIMING, MF_BYCOMMAND | MF_UNCHECKED);
					btnInfo.pszText = TEXT("Start/Stop");
					}
				else
					{
					CheckMenuItem(hMenu, IDM_SPLIT_TIMING, MF_BYCOMMAND | MF_CHECKED);
					btnInfo.pszText = TEXT("Start/Split");
					}
				SendMessage(Global.menuBar, TB_SETBUTTONINFO, 
					(LPARAM)IDM_STARTSPLITSTOP, (LPARAM)&btnInfo);
				#pragma endregion
				break;

			case IDM_STARTSPLITSTOP:
				#pragma region Split button
				if( Global.time.clockMode )
					SendMessage(hwnd, WM_COMMAND, (WPARAM)IDM_SPLIT, 0);
				else if( !Global.time.timerRunning )
					SendMessage(hwnd, WM_COMMAND, (WPARAM)IDM_START, 0);
				else
					{
					if( isSplitTiming() )
						SendMessage(hwnd, WM_COMMAND, (WPARAM)IDM_SPLIT, 0);
					else
						SendMessage(hwnd, WM_COMMAND, (WPARAM)IDM_STOP, 0);
					}
				#pragma endregion
				break;

			case IDM_STARTSTOP:
				#pragma region start/stop toggle
				if( !Global.time.timerRunning )
					SendMessage(hwnd, WM_COMMAND, (WPARAM)IDM_START, 0);
				else
					SendMessage(hwnd, WM_COMMAND, (WPARAM)IDM_STOP, 0);
				#pragma endregion
				break;

			case IDM_START:
				#pragma region Stopwatch start
				if( !Global.time.timerRunning )
					{
					Global.time.epoch = getSystemMillis();
						//GetTickCount();
					SetTimer(hwnd, 2, 50, NULL);
					Global.time.timerRunning = true;
					}
				#pragma endregion
				break;

			case IDM_STOP:
				#pragma region Stopwatch stop
				if( Global.time.timerRunning )
					{
					KillTimer(hwnd, 2);
					Global.time.totalElapsed += getSystemMillis() - Global.time.epoch;
					Global.time.timerRunning = false;
					}
				#pragma endregion
				break;

			case IDM_SPLIT:
				#pragma region Timer split
				if( Global.time.timerRunning )
					{
					TCHAR timeStr[] = TEXT("mm'ss\"uuu------");				
					DWORD timeElapsed = Global.time.totalElapsed + 
						getSystemMillis() - Global.time.epoch;
					DWORD mins = timeElapsed / (1000 * 60) ;
					timeElapsed -= mins * 1000 * 60;
					DWORD secs = timeElapsed / 1000 ;
					timeElapsed -= secs * 1000;
					DWORD usec = timeElapsed / 1;
					wsprintf(timeStr, TEXT("%02i'%02i\"%03i"), mins,secs,usec);
					int i = SendMessage(Global.timeList, LB_INSERTSTRING, -1, (LPARAM)timeStr);
					SendMessage(Global.timeList, LB_SETCURSEL, i, 0);
					}
				else if( Global.time.clockMode )
					{
					TCHAR timeStr[] = TEXT("hh:mm:ss:msms");				
					SYSTEMTIME sysTime;
					GetLocalTime(&sysTime);
					wsprintf(timeStr, TEXT("%02i:%02i:%02i"),
						sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
					int i = SendMessage(Global.timeList, LB_INSERTSTRING, -1, (LPARAM)timeStr);
					SendMessage(Global.timeList, LB_SETCURSEL, i, 0);
					}
				#pragma endregion
				break;

			case IDM_RESET:
				#pragma region Reset timer
				if( Global.time.timerRunning )
					{
					KillTimer(hwnd, 2);
					Global.time.timerRunning = false;
					}
				Global.time.totalElapsed = 0;
				if( !Global.time.clockMode )
					SendMessage(hwnd, WM_TIMER, (WPARAM)3, 0);
				iIndex = SendMessage(Global.timeList, LB_GETCOUNT, 0, 0);
				while( iIndex-- )
					SendMessage(Global.timeList, LB_DELETESTRING, 0, 0);
				#pragma endregion
				break;

            default:
                return DefWindowProc(hwnd, msg, wParam, lParam);
			}
		return 0;
	}	
// Should not happen
return DefWindowProc(hwnd, msg, wParam, lParam);
#pragma endregion
}


LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#pragma region
RECT windowRect;
int windowWidth;
int windowHeight;
switch( msg )
	{
	case WM_COMMAND:
		return CommandProc(hwnd,msg,wParam,lParam);
	case WM_PAINT:
		return PaintProc(hwnd,msg,wParam,lParam);
	case WM_TIMER:
		return TimerProc(hwnd,msg,wParam,lParam);

	case WM_CREATE:
		#pragma region Menubar
		SHMENUBARINFO menuInfo;
		memset(&menuInfo, 0, sizeof(SHMENUBARINFO));
		menuInfo.cbSize     = sizeof(SHMENUBARINFO);
		menuInfo.hInstRes   = Global.mainWnd.hInst;
		menuInfo.hwndParent = hwnd;
		menuInfo.nToolBarId = IDR_MENU;
		menuInfo.dwFlags    = SHCMBF_HIDESIPBUTTON;
		if( SHCreateMenuBar( &menuInfo ) )
			Global.menuBar = menuInfo.hwndMB;
		else
			Global.menuBar = NULL;
		#pragma endregion (either side of input panel button)

		// Input panel
		memset(&Global.inputPanel, 0, sizeof(Global.inputPanel));
		Global.inputPanel.cbSize = sizeof(Global.inputPanel);

		#pragma region Split time list
		Global.timeList = CreateWindow(TEXT("listbox"), NULL,
			WS_CHILD|WS_VISIBLE|WS_BORDER|WS_VSCROLL,
//			WS_CHILD|WS_VISIBLE|WS_BORDER|ES_MULTILINE|WS_VSCROLL,
			0,0,100,100, hwnd, NULL, 
			Global.mainWnd.hInst, NULL);
			//((LPCREATESTRUCT)lParam)->hInstance,NULL);
		if(! Global.timeList )
			return FALSE;
		#pragma endregion

		#pragma region Drawing colours
		Global.pen   = CreatePen(PS_SOLID, 1, RGB(0,255,0) );
		Global.brush[0] = CreateSolidBrush( RGB(255,0,0) );
		Global.brush[1] = CreateSolidBrush( RGB(0,0,0) );
		#pragma endregion
		
		#pragma region Display segments		
		//Global.time.suspendedTimer = false;
		//GetCurrentFT(&Global.time.bigBang);
		Global.time.clockMode = false;
		Global.time.timerRunning = false;
		Global.time.totalElapsed = 0;
		Global.memDC = NULL;
		GetWindowRect(hwnd,&Global.digitBox);
		Global.digitBox.top = 0;
		Global.digitBox.bottom = 
			calcDigitCoords( Global.digitBox.right - Global.digitBox.left );
		SendMessage(hwnd, WM_TIMER, (WPARAM)3, 0);
		#pragma endregion
		return 0;

	case WM_SIZE:
		#pragma region Window resize
		GetWindowRect(hwnd,&Global.digitBox);
		windowWidth = Global.digitBox.right - Global.digitBox.left;
		windowHeight= Global.digitBox.bottom - Global.digitBox.top;

		Global.digitBox.top = 0;
		Global.digitBox.bottom = calcDigitCoords( windowWidth );
		DeleteObject(Global.memDCbitmap);
		DeleteDC(Global.memDC);
		Global.memDC = NULL;

		windowHeight = windowHeight - Global.digitBox.bottom;
		MoveWindow(Global.timeList, 0, Global.digitBox.bottom,
			windowWidth / 2, windowHeight, TRUE);
		GetWindowRect(Global.timeList, &windowRect);
		windowHeight = windowHeight - (windowRect.bottom - windowRect.top);

		MoveWindow(Global.timeList, (windowHeight/2), Global.digitBox.bottom + (windowHeight/2),
			windowWidth / 2 - (windowHeight/2), (windowRect.bottom - windowRect.top), TRUE);
		#pragma endregion
		return 0;

	case WM_ACTIVATE:
		SHHandleWMActivate(hwnd, wParam, lParam, &Global.inputPanel, FALSE);
		return 0;

	case WM_SETTINGCHANGE:
		SHHandleWMSettingChange(hwnd, wParam, lParam, &Global.inputPanel);
        return 0;

	case WM_DESTROY:
		CommandBar_Destroy(Global.menuBar);
		DeleteObject(Global.pen);
		PostQuitMessage(0);
		return 0;

	case WM_HIBERNATE:
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
return 0;
#pragma endregion
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR argv, int argc)
{
#pragma region
MSG msg;
WNDCLASS wc;
HWND hwnd;
const TCHAR wndClassName[] = TEXT("TIMERWINCE");
const TCHAR windowTitle[]  = TEXT("TimerCE");

Global.mainWnd.hInst = hInst;
SHInitExtraControls();
if( (hwnd = FindWindow(wndClassName, windowTitle)) )
    { //Already running
    SetForegroundWindow( (HWND)((ULONG)hwnd | 0x1) );
    return FALSE;
	}
	
memset(&wc, 0, sizeof(WNDCLASS));
wc.style         = CS_HREDRAW | CS_VREDRAW;
wc.lpfnWndProc   = MainWndProc;
wc.hInstance     = hInst;
wc.hIcon=NULL;
//wc.hIcon         = LoadIcon(hInst, MAKEINTRESOURCE(IDI_NOTEPADCE));
wc.hCursor       = 0; // LoadCursor(NULL,IDC_ARROW);
wc.hbrBackground = (HBRUSH) GetStockObject(LTGRAY_BRUSH);
wc.lpszClassName = wndClassName;
if(RegisterClass(&wc)==0)
   return FALSE;

hwnd = CreateWindow(wndClassName, windowTitle, WS_VISIBLE,
		    CW_USEDEFAULT,CW_USEDEFAULT, //x,y pos
		    CW_USEDEFAULT,CW_USEDEFAULT, //width,height,
		    NULL, //parent
		    NULL, //menu (must be null)
		    hInst, NULL);
if( !hwnd )
	return FALSE;

if (Global.menuBar)
    {
	RECT windowRect, menuRect;
	GetWindowRect(hwnd,&windowRect);
	GetWindowRect(Global.menuBar,&menuRect);
	windowRect.bottom = windowRect.bottom - (menuRect.bottom - menuRect.top);
	MoveWindow(hwnd, 
			   windowRect.left, windowRect.top,  // Position
			   windowRect.right-windowRect.left, // Width 
			   windowRect.bottom-windowRect.top, // Height
			   FALSE // Don't repaint
			   );
	}
else
	return FALSE;

ShowWindow(hwnd, argc);
UpdateWindow(hwnd);
HACCEL hAccelTable = LoadAccelerators(hInst, 
		MAKEINTRESOURCE(IDC_STOPWATCHCE));

while (GetMessage(&msg, NULL, 0, 0)) 
	if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
		}
return (int) msg.wParam;
#pragma endregion
}
