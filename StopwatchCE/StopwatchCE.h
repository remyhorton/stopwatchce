/*
  StopwatchCE - Application header file
  SPDX-License-Identifier: GPL-3.0-only
  Copyright(c) Remy Horton, 2018
*/
#pragma once
#include "resourceppc.h"



typedef struct
	{
	POINT segments[7][4];
} SegSet_t;

typedef struct 
	{
	struct 
		{
		HWND hwnd;
		HINSTANCE hInst; 
	} mainWnd;
	HWND menuBar;
	HMENU menuAlt;
	SHACTIVATEINFO inputPanel;
	LPBITMAPINFO bmInfo;
	HPEN pen;
	HBRUSH brush[2];
	SegSet_t digitCoords[6];
	unsigned char digitBits[6];
	RECT     digitBox;
	
	struct
		{
		//SYSTEMTIME epoch;
		bool clockMode;
		bool timerRunning;
		//bool suspendedTimer;
		DWORD epoch;
		DWORD totalElapsed;
		//FILETIME bigBang;
	} time;

	HDC memDC;
	HBITMAP memDCbitmap;

	HWND timeList;


} Global_t;

extern Global_t Global;

// About.cpp
INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);

// Display.cpp
LRESULT CALLBACK PaintProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK TimerProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
int     calcDigitCoords(const int windowWidth);

// StopwatchCE.cpp
DWORD getSystemMillis();
