StopwatchCE: Stopwatch for Windows Mobile
=========================================
StopwatchCE is a stopwatch utility for Windows Mobile (WindowsCE), which
was written because my mobile at the time did not include a decent timing
program. It is not intended to be anything fancy, and currently includes:

* Split timing
* Start/stop timing
* Digital clock mode


Current Status
--------------
I don't use my Xperia X1 (or any other Windows Mobile device) anymore,
so this application is no longer actively maintained.


Available builds
----------------
StopwatchCE is primarily written for Windows Mobile v6.1, as that is the
version my mobile handset runs. I used a v6.0 emulator during development,
so it should be fine with v6.0 (and later, such as v6.5) handsets as well.
I have also compiled binaries for earlier versions but I have only tested
these versions briefly using emulators.

* PocketPC 2003
* Smartphone 2003
* PocketPC v5.0
* Smartphone v5.0
* PocketPC v6.0/6.1

To the best of my knowledge StopwatchCE is incompatible with Windows Phone
7 and its descendents.

Screeenshots
------------
Screenshots are from v1.1beta (taken early-2010) which used *TimerCE* as a
name internally.


![Screenshot](./screenshots/ppc2003.png)
![Screenshot](./screenshots/smart50.png)
![Screenshot](./screenshots/wm60.png)


Licence
-------
StopwatchCE is licenced under [version 3 of the GPL][gpl3].


Contact
-------
Send emails to ``remy.horton`` (at) ``gmail.com``


[gpl3]: https://www.gnu.org/licenses/gpl.html

